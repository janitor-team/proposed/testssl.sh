#!/bin/sh
set -e
set -u

export LC_ALL=C.UTF-8

cp -av etc "$AUTOPKGTEST_TMP"
cp -av t "$AUTOPKGTEST_TMP"

cd "$AUTOPKGTEST_TMP"
sed -i s@./testssl.sh@/usr/bin/testssl@g t/*.t

# Only run tests 00-05 as others require network access.
prove -v t/0[0-5]*
